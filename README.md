
<img src="https://raw.githubusercontent.com/redox-os/assets/master/logos/sodium/Sodium_logo.png" height="200" />

## Sodium: Vim 2.0

**Sodium** is an editor inspired by Vim (but not a clone). It aims to be efficient, fast, and productive.

### Salty Bois TODO

**Vim Mode**
| Task          | File          | Action
|--|--|--|
| Key Press     | core/exec     | Use Editor.key_map for bindings
| Editor State  | state/editor  | Init Editor.key_map
| Options       | state/options | Add Options.vim_mode
| Key Binds     | io/key        | Add KeyBind definitions (enum)

**Copy / Paste**
| Task            | File            | Action
|--|--|--|
| Editor State    | state/editor    | Init Editor.clipboard buffer
| Key Press       | core/exec       | Extend binds for 'copy'/'paste'
| Copy on Delete  | edit/selection  | Add removed chars to clipboard
| Copy            | edit/selection  | Add function to copy to clipboard

| Binding         | Description
|--|--|
| y{motion}       | Yank where 'motion' is a sodium motion
| p               | paste before the cursor
| P               | paste after the cursor


### Library Requirements

Sodium requires the sdl2 library in order to build.
To install on Ubuntu, use the following command: `sudo apt-get install libsdl2-dev`

### Build

Use `cargo run --features orbital` in order to build the program.
