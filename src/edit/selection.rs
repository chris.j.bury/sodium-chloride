use edit::buffer::TextBuffer;
use state::editor::Editor;

impl Editor {
    /// Remove from a given motion (row based), i.e. if the motion given is to another line, all
    /// the lines from the current one to the one defined by the motion are removed. If the motion
    /// defines a position on the same line, only the characters from the current position to the
    /// motion's position are removed.
    pub fn remove_rb<'a>(&mut self, (x, y): (isize, isize)) {
        if y == (self.y() as isize) {
            let (x, y) = self.bound((x as usize, y as usize), false);
            // Single line mode
            let (a, b) = if self.x() > x {
                (x, self.x())
            } else {
                (self.x(), x)
            };

            let clip = self.buffers.current_buffer_mut()[y].drain(a..b).collect::<String>(); 
            self.clipboard.set(&clip);
        } else {
            let (_, y) = self.bound((x as usize, y as usize), true);
            // Full line mode
            let (a, b) = if self.y() < y {
                (self.y(), y)
            } else {
                (y, self.y())
            };

            // TODO: Make this more idiomatic (drain)
            self.clipboard.clear();
            for _ in a..=b {
                if self.buffers.current_buffer().len() > 1 {
                    let line = self.buffers.current_buffer_mut().remove_line(a);
                    self.clipboard.push_line(&line);
                } else {
                    self.buffers.current_buffer_mut()[0] = String::new();
                }
            }
        }

        self.hint();
    }
       
    /// Yank/Copy from a given motion (row based), i.e. if the motion given is to another line, all
    /// the lines from the current one to the one defined by the motion are removed. If the motion
    /// defines a position on the same line, only the characters from the current position to the
    /// motion's position are copied.
    pub fn yank_rb<'a>(&mut self, (x, y): (isize, isize)) {
        if y == (self.y() as isize) {
            let (x, y) = self.bound((x as usize, y as usize), false);
            // Single line mode
            let (a, b) = if self.x() > x {
                (x, self.x())
            } else {
                (self.x(), x)
            };


            let clip = &self.buffers.current_buffer_mut()[y][a..b]; 
            self.clipboard.set(clip);
        } else {
            let (_, y) = self.bound((x as usize, y as usize), true);
            // Full line mode
            let (a, b) = if self.y() < y {
                (self.y(), y)
            } else {
                (y, self.y())
            };

            self.clipboard.clear();
            for i in a..=b {
                if self.buffers.current_buffer().len() > 1 {
                    if let Some(line) = self.buffers.current_buffer_mut().get_line(i) {
                        self.clipboard.push_line(line);
                    }
                } else {
                    self.buffers.current_buffer_mut()[0] = String::new();
                }
            }
        }

        // self.hint();
    }
}
