use state::editor::Editor;
use io::parse::{Inst, Parameter};
use state::mode::{CommandMode, Mode, PrimitiveMode};
use edit::insert::{InsertMode, InsertOptions};
use io::redraw::RedrawTask;
use edit::buffer::TextBuffer;
use core::prompt::PromptCommand;
use io::key_map::Action;

// TODO: Move the command definitions outta here
impl Editor {
    /// Execute an instruction
    pub fn exec(&mut self, Inst(para, cmd): Inst) {
        use io::key::Key::*;
        use state::mode::Mode::*;
        use state::mode::PrimitiveMode::*;
        use state::mode::CommandMode::*;

        let n = para.d();
        let bef = self.pos();
        let mut mov = false;

        match (self.cursor().mode, self.key_map.reverse_lookup(cmd.key)) {
            (Primitive(Prompt), Action::Spacebar) if self.key_state.shift => {
                self.prompt.insert(0, String::new());
                self.cursor_mut().mode = Mode::Command(CommandMode::Normal);
            }
            (Primitive(Insert(_)), Action::Escape) => {
                let left = self.left(1);
                self.goto(left);
                self.cursor_mut().mode = Mode::Command(CommandMode::Normal);
            }
            (Primitive(Insert(_)), Action::Spacebar) if self.key_state.shift => {
                let left = self.left(1);
                self.goto(left);
                self.cursor_mut().mode = Mode::Command(CommandMode::Normal);
            }
            (_, Action::Spacebar) if self.key_state.shift => {
                self.cursor_mut().mode = Mode::Command(CommandMode::Normal)
            }
            (_, Action::Spacebar) if self.key_state.alt => self.next_cursor(),
            _ if self.key_state.alt => if let Some(m) = self.to_motion(Inst(para, cmd)) {
                self.goto(m);
            },
            (Command(Normal), Action::Insert) => {
                self.cursor_mut().mode = Mode::Primitive(PrimitiveMode::Insert(InsertOptions {
                    mode: InsertMode::Insert,
                }));
            }
            (Command(Normal), Action::AltInsert) => {
                self.cursor_mut().x = 0;
                self.cursor_mut().mode = Mode::Primitive(PrimitiveMode::Insert(InsertOptions {
                    mode: InsertMode::Insert,
                }));
            }
            (Command(Normal), Action::Append) => {
                let pos = self.right(1, false);
                self.goto(pos);
                self.cursor_mut().mode = Mode::Primitive(PrimitiveMode::Insert(InsertOptions {
                    mode: InsertMode::Insert,
                }));
            }
            (Command(Normal), Action::AltAppend) => {
                let pos = (self.buffers.current_buffer()[self.y()].len(), self.y());
                //let pos = self.right(1, false);
                self.goto(pos);
                self.cursor_mut().mode = Mode::Primitive(PrimitiveMode::Insert(InsertOptions {
                    mode: InsertMode::Insert,
                }));
            }
            (Command(Normal), Action::NewLine) => {
                let y = self.y();
                let ind = if self.options.autoindent {
                    self.buffers.current_buffer().get_indent(y).to_owned()
                } else {
                    String::new()
                };
                let last = ind.len();
                self.buffers
                    .current_buffer_mut()
                    .insert_line(y + 1, ind.into());
                self.goto((last, y + 1));
                self.cursor_mut().mode = Mode::Primitive(PrimitiveMode::Insert(InsertOptions {
                    mode: InsertMode::Insert,
                }));
            }
            (Command(Normal), Action::MoveLeft) => {
                let left = self.left(n);
                self.goto(left);
                mov = true;
            }
            (Command(Normal), Action::MoveDown) => {
                let down = self.down(n);
                self.goto(down);
                mov = true;
            }
            (Command(Normal), Action::MoveUp) => {
                let up = self.up(n);
                self.goto(up);
                mov = true;
            }
            (Command(Normal), Action::MoveRight) => {
                let right = self.right(n, true);
                self.goto(right);
                mov = true;
            }
            (Command(Normal), Action::AltMoveDown) => {
                let down = self.down(15 * n);
                self.goto(down);
                mov = true;
            }
            (Command(Normal), Action::AltMoveUp) => {
                let up = self.up(15 * n);
                self.goto(up);
                mov = true;
            }
            (Command(Normal), Action::Remove) => {
                self.delete();
                let bounded = self.bound(self.pos(), true);
                self.goto(bounded);
            }
            (Command(Normal), Action::AltRemove) => {
                self.backspace();
                let bounded = self.bound(self.pos(), true);
                self.goto(bounded);
            }
            (Command(Normal), Action::EndOfLine) => {
                if self.buffers.current_buffer()[self.y()].len() != 0 {
                    let ln_end = (self.buffers.current_buffer()[self.y()].len() - 1, self.y());
                    self.goto(ln_end);
                    mov = true;
                }
            }
            (Command(Normal), Action::StartOfLine) => {
                self.cursor_mut().x = 0;
                mov = true;
            }
            (Command(Normal), Action::Replace) => {
                let (x, y) = self.pos();
                let c = self.get_char();
                let current_buffer = self.buffers.current_buffer_info_mut();
                // If there is nothing in the current buffer
                // ignore the command
                if current_buffer.raw_buffer[y].len() > 0 {
                    current_buffer.raw_buffer[y].remove(x);
                }
                current_buffer.raw_buffer[y].insert(x, c);
            }
            (Command(Normal), Action::ReplaceMode) => {
                self.cursor_mut().mode = Mode::Primitive(PrimitiveMode::Insert(InsertOptions {
                    mode: InsertMode::Replace,
                }));
            }
            (Command(Normal), Action::Delete) => {
                let ins = self.get_inst();
                if let Action::Delete = self.key_map.reverse_lookup(ins.1.key) {
                    let y = self.y();
                    let line = &self.buffers.current_buffer_mut().remove_line(y);

                    self.clipboard.set_line(line);
                } else if let Some(m) = self.to_motion_unbounded(ins) {
                    self.remove_rb(m);
                }
            }
            (Command(Normal), Action::Change) => {
                let ins = self.get_inst();
                if let Some(m) = self.to_motion_unbounded(ins) {
                    self.remove_rb(m);
                    self.cursor_mut().mode = Mode::Primitive(PrimitiveMode::Insert(InsertOptions {
                        mode: InsertMode::Insert,
                    }));
                }
            }
            (Command(Normal), Action::GoToEnd) => {
                let last = self.buffers.current_buffer().len() - 1;
                self.goto((0, last));
                mov = true;
            }
            (Command(Normal), Action::GoTo) => {
                if let Parameter::Int(n) = para {
                    self.goto((0, n.wrapping_sub(1)));
                    mov = true;
                } else {
                    let inst = self.get_inst();
                    if let Some(m) = self.to_motion(inst) {
                        self.goto(m); // fix
                        mov = true;
                    }
                }
            }
            (Command(Normal), Action::Branch) => {
                // Branch cursor
                if self.buffers.current_buffer_info().cursors.len() < 255 {
                    let cursor = self.cursor().clone();
                    let current_cursor_index =
                        self.buffers.current_buffer_info().current_cursor as usize;
                    self.buffers
                        .current_buffer_info_mut()
                        .cursors
                        .insert(current_cursor_index, cursor);
                    self.next_cursor();
                } else {
                    self.status_bar.msg = format!("At max 255 cursors");
                }
            }
            (Command(Normal), Action::UnBranch) => {
                // Delete cursor
                if self.buffers.current_buffer_info().cursors.len() > 1 {
                    let current_cursor_index = self.buffers.current_buffer_info().current_cursor;
                    self.buffers
                        .current_buffer_info_mut()
                        .cursors
                        .remove(current_cursor_index as usize);
                    self.prev_cursor();
                } else {
                    self.status_bar.msg = format!("No other cursors!");
                }
            }
            (Command(Normal), Action::NextOccur) => {
                let ch = self.get_char();

                let pos = self.next_ocur(ch, n);
                if let Some(p) = pos {
                    let y = self.y();
                    self.goto((p, y));
                    mov = true;
                }
            }
            (Command(Normal), Action::PrevOccur) => {
                let ch = self.get_char();

                let pos = self.previous_ocur(ch, n);
                if let Some(p) = pos {
                    let y = self.y();
                    self.goto((p, y));
                    mov = true;
                }
            }
            (Command(Normal), Action::NextWord) => {
                let pos = self._next_word_forward(n);
                if let Some(p) = pos {
                    let y = self.y();
                    self.goto((p, y));
                    mov = true;
                }
            }
            (Command(Normal), Action::PromptMode) => {
                self.cursor_mut().mode = Mode::Primitive(PrimitiveMode::Prompt)
            }
            (Command(Normal), Action::Spacebar) => self.next_cursor(),
            (Command(Normal), Action::Scroll) => {
                let Inst(param, cmd) = self.get_inst();
                match param {
                    Parameter::Null => if let Some(m) = self.to_motion(Inst(param, cmd)) {
                        self.buffers.current_buffer_info_mut().scroll_y = m.1;
                        self.goto(m);
                    },
                    Parameter::Int(n) => {
                        self.buffers.current_buffer_info_mut().scroll_y = n;
                    }
                }
                self.redraw_task = RedrawTask::Full;
            }
            (Command(Normal), Action::AltScroll) => {
                self.buffers.current_buffer_info_mut().scroll_y = self.y() - 3;
                self.redraw_task = RedrawTask::Full;
            }
            (Command(Normal), Action::CounterPart) => {
                self.invert_chars(n);
            }
            (Command(Normal), Action::RepeatLast) => if let Some(inst) = self.previous_instruction {
                self.exec(inst);
            } else {
                self.status_bar.msg = "No previous command".into();
                self.redraw_task = RedrawTask::StatusBar;
            },
            (Command(Normal), Action::Yank) => {
                let ins = self.get_inst();
                if let Action::Yank = self.key_map.reverse_lookup(ins.1.key) {
                    let y = self.y();
                    let line = &self.buffers.current_buffer_mut().get_line(y).unwrap();

                    self.clipboard.set_line(line);
                } else if let Some(m) = self.to_motion_unbounded(ins) {
                    self.yank_rb(m);
                }
                self.status_bar.msg = format!("Fat Yank");
            }
            (Command(Normal), Action::Put) => {
                let (x, y) = self.pos();
                
                if self.clipboard.inline {
                    // Insert inline
                    let line_buf = &mut self.buffers.current_buffer_mut()[y];
                    let clip_buf = self.clipboard.get();
                    if !clip_buf.is_empty() {
                        for (i, c) in clip_buf.trim().chars().enumerate() {
                            line_buf.insert(x + i, c);   
                        }
                    }
                } else {
                    // Insert lines starting on next line
                    for (i, line) in self.clipboard.get().lines().enumerate() {
                        self.buffers.current_buffer_mut().insert_line(y + i + 1, line.to_string());
                    }

                    // Move to next line
                    let left = self.down(n);
                    self.goto(left);
                    mov = true;
                }
                self.status_bar.msg = format!("Put put");
            }
            (Command(Normal), Action::Unrecognized(c)) => {
                self.status_bar.msg = format!("Unknown command: {}", c);
                self.redraw_task = RedrawTask::StatusBar;
            }
            (Primitive(Insert(opt)), _) => {
                match cmd.key {
                    Up => {
                        let up = self.up(n);
                        self.goto(up);
                        mov = true;
                    }
                    Down => {
                        let down = self.down(n);
                        self.goto(down);
                        mov = true;
                    }
                    Left => {
                        let left = self.left(n);
                        self.goto(left);
                        mov = true;
                    }
                    Right => {
                        let right = self.right(n, true);
                        self.goto(right);
                        mov = true;
                    }
                    _ => self.insert(cmd.key, opt),
                }
                
                
            }
            (Primitive(Prompt), Action::Enter) => {
                self.cursor_mut().mode = Command(Normal);
                if let Some(cmd) = PromptCommand::parse(&self.prompt[self.prompt_index].clone()) {
                    self.invoke(cmd);
                    self.redraw_task = RedrawTask::StatusBar;
                } else {
                    self.status_bar.msg = format!("Unknown command: {}", self.prompt[self.prompt_index]);
                }
    
                // If we use a command we used before, don't add a new line to the vec
                let cmd = self.prompt[self.prompt_index].clone();
                if self.prompt_index != 0 {
                    self.prompt[0] = cmd;
                }
                // Don't insert anything if the user didn't write anything
                if self.prompt[self.prompt_index] != "" {
                    self.prompt.insert(0, String::new());
                }
                self.prompt_index = 0;
            }
            (Primitive(Prompt), Action::Backspace) => {
                self.prompt[self.prompt_index].pop();
                self.redraw_task = RedrawTask::StatusBar;
            }
            (Primitive(Prompt), Action::MoveUp) => {
                if self.prompt_index < self.prompt.len() - 1 {
                    self.prompt_index += 1;
                }
                self.redraw_task = RedrawTask::StatusBar;
            }
            (Primitive(Prompt), Action::MoveDown) => {
                if self.prompt_index > 0 {
                    self.prompt_index -= 1;
                }
                self.redraw_task = RedrawTask::StatusBar;
            }
            (Primitive(Prompt), _) => {
                match cmd.key {
                    Char(c) => {
                        self.prompt[self.prompt_index].push(c);
                        self.redraw_task = RedrawTask::StatusBar;
                    }
                    _ => {
                        self.status_bar.msg = format!("Unknown command");
                        self.redraw_task = RedrawTask::StatusBar;
                    }
                }
            }
            _ => {
                self.status_bar.msg = format!("Unknown command");
                self.redraw_task = RedrawTask::StatusBar;
            }
        }


        






        if mov {
            self.redraw_task = RedrawTask::Cursor(bef, self.pos());
        }

        if !(self.cursor().mode == Command(Normal) && cmd.key == Char('.')) {
            self.previous_instruction = Some(Inst(para, cmd));
        }
    }
}
